export function isDebug() {
  return !process.env.NODE_ENV || process.env.NODE_ENV === "development";
}

export function isOwner(user, page) {
  if (user.user && page.owner) return page.owner.id === user.user.id;
  else return false;
}
