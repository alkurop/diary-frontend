/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateUser = `subscription OnCreateUser {
  onCreateUser {
    id
    picture
    name
    createdAt
    username
    email
  }
}
`;
export const onUpdateUser = `subscription OnUpdateUser {
  onUpdateUser {
    id
    picture
    name
    createdAt
    username
    email
  }
}
`;
export const onDeleteUser = `subscription OnDeleteUser {
  onDeleteUser {
    id
    picture
    name
    createdAt
    username
    email
  }
}
`;
export const onCreateSettings = `subscription OnCreateSettings {
  onCreateSettings {
    ownerId
    content
  }
}
`;
export const onUpdateSettings = `subscription OnUpdateSettings {
  onUpdateSettings {
    ownerId
    content
  }
}
`;
export const onDeleteSettings = `subscription OnDeleteSettings {
  onDeleteSettings {
    ownerId
    content
  }
}
`;
export const onCreateContent = `subscription OnCreateContent {
  onCreateContent {
    id
    contentType
    content
    createdAt
    updatedAt
    storyId
  }
}
`;
export const onUpdateContent = `subscription OnUpdateContent {
  onUpdateContent {
    id
    contentType
    content
    createdAt
    updatedAt
    storyId
  }
}
`;
export const onDeleteContent = `subscription OnDeleteContent {
  onDeleteContent {
    id
    contentType
    content
    createdAt
    updatedAt
    storyId
  }
}
`;
export const onCreateStory = `subscription OnCreateStory {
  onCreateStory {
    id
    published
    createdAt
    updatedAt
    title
    ownerId
    bookId
  }
}
`;
export const onUpdateStory = `subscription OnUpdateStory {
  onUpdateStory {
    id
    published
    createdAt
    updatedAt
    title
    ownerId
    bookId
  }
}
`;
export const onDeleteStory = `subscription OnDeleteStory {
  onDeleteStory {
    id
    published
    createdAt
    updatedAt
    title
    ownerId
    bookId
  }
}
`;
export const onCreateComment = `subscription OnCreateComment {
  onCreateComment {
    id
    storyId
    updatedAt
    contentType
    content
    userId
    userName
  }
}
`;
export const onUpdateComment = `subscription OnUpdateComment {
  onUpdateComment {
    id
    storyId
    updatedAt
    contentType
    content
    userId
    userName
  }
}
`;
export const onDeleteComment = `subscription OnDeleteComment {
  onDeleteComment {
    id
    storyId
    updatedAt
    contentType
    content
    userId
    userName
  }
}
`;
export const onCreateEvent = `subscription OnCreateEvent {
  onCreateEvent {
    id
    content
    type
    ownerId
    createdAt
    viewed
  }
}
`;
export const onUpdateEvent = `subscription OnUpdateEvent {
  onUpdateEvent {
    id
    content
    type
    ownerId
    createdAt
    viewed
  }
}
`;
export const onDeleteEvent = `subscription OnDeleteEvent {
  onDeleteEvent {
    id
    content
    type
    ownerId
    createdAt
    viewed
  }
}
`;
