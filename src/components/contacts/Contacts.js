//libs
import React from "react";
import { connect } from "react-redux";
import Avatar from "@material-ui/core/Avatar";
import _ from "lodash";
import { useHistory } from "react-router-dom";
import { useTheme } from "@material-ui/core/styles";

//comps
import { getUsersListApi } from "../../api_actions/userApiActions";
import "./Contacts.scss";
import { ErrorAction } from "../../reducers/errorReducer";
import "../../api_actions/userApiActions";
import { PageAction } from "../../reducers/pageReducer";
import { UserCacheAction } from "../../reducers/usersCacheReducer";

function Contacts({ updateUserCache, onGetErr, switchPage, filterId }) {
  const [userList, setUserList] = React.useState(null);
  const history = useHistory();

  React.useEffect(() => {
    if (!userList) {
      getUsersListApi()
        .then((userList) => {
          let list = _.sortBy(userList, ["createdAt"], ["desc"]);
          if (filterId) {
            list = _.filter(list, (item) => {
              return item.id !== filterId;
            });
          }
          setUserList(list);
          updateUserCache(list);
        })
        .catch((err) => {
          onGetErr(err);
        });
    }
  });

  function navigateToUserPage(user) {
    history.push(`/p/${user.id}`);
  }
  const theme = useTheme();

  const color = theme.overrides.divider;
  const border = theme.overrides.border;
  const styleHeader = {
    background: color,
    border: border,
  };
  const styleItem = {
    borderLeft: border,
    borderRight: border,
    borderBottom: border,
  };

  let userListMap = null;
  if (userList) {
    userListMap = userList.map((item) => {
      return (
        <div
          className="PageSideFragmentItem"
          style={styleItem}
          key={item.id}
          onClick={() => {
            navigateToUserPage(item);
            switchPage(item);
          }}
        >
          <Avatar src={item.picture} />
          <div className="ContactName">{item.name}</div>
        </div>
      );
    });
  }

  return (
    <div className="Contacts">
      <div className="PageSideFragmentHeader" style={styleHeader}>
        Users
      </div>
      <div className="PageSideFragmentContentList">{userListMap}</div>
    </div>
  );
}

export default connect(
  (state) => ({}),
  (dispatch) => ({
    updateUserCache: (userList) => {
      dispatch({
        type: UserCacheAction.AddItems,
        items: userList,
      });
    },
    switchPage: (user) => {
      dispatch({
        type: PageAction.Switch,
        user: user,
      });
    },
    onGetErr: (err) => {
      dispatch({
        type: ErrorAction.OnError,
        payload: {
          err: err,
          message: "Get users failed",
        },
      });
    },
  })
)(Contacts);
