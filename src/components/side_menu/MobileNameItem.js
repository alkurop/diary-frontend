import React from "react";

export default function MobileNameItem(props) {
  return <div className="MobileNameItem">{props.name}</div>;
}
