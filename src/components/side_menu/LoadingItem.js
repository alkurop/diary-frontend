import React from "react";

export default function LoadingItem({ className }) {
  return (
    <div className={className ? className : "LoadingItem"}>
      <div className="PageSideFragmentItemText"> Loading ...</div>
    </div>
  );
}
