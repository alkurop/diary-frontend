import React from "react";
import "./InputWithChecker.scss";

function InputWithChecker({
  checker,
  inputType,
  onInputChange,
  placeholder,
  subtitle,
  defaultValue,
  className,
  inputClassName
}) {
  const [checked, setChecked] = React.useState(null);
  function onChange(event) {
    const newText = event.target.value;
    const check = checker(newText);
    onInputChange(check);
    setChecked(check);
  }
  return (
    <div className={className ? className : "InputWithCheckerContainer"}>
      {subtitle ? (
        <div className="InputWithCheckerSubtitle">{subtitle}</div>
      ) : null}
      <input
        className={inputClassName}
        type={inputType ? inputType : "text"}
        placeholder={placeholder}
        onChange={onChange}
        defaultValue={defaultValue}
      />
      {checked && !checked.correct && checked.error ? (
        <div className="InputWithCheckerError">{checked.error}</div>
      ) : null}
    </div>
  );
}

export default InputWithChecker;
