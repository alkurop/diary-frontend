import React from "react";

export default function ReloadComponent(props) {
  return (
    <div className={props.className} onClick={props.onClick}>
      Failed ... Reload
    </div>
  );
}
