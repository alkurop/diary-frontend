//libs
import React, { Fragment } from "react";
import Avatar from "@material-ui/core/Avatar";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";

//comps
import "./LoginComponent.scss";
import "./LogoutButton";
import LogoutButton from "./LogoutButton";

export default function AccountDetails({ user, settings }) {
  const [modal, setModal] = React.useState(false);

  function toggle() {
    setModal(!modal);
  }

  return (
    <Fragment>
      <div className="UserAndAvatar">
        <Avatar className="Avatar" src={user.picture} onClick={toggle} />
      </div>
      <Dialog open={modal} onClose={toggle}>
        <DialogTitle id="alert-dialog-title">Account</DialogTitle>
        <DialogContent>
          <Fragment>
            <div className="UserName">Name: {user.name}</div>
            <div className="DialogActions">
            <LogoutButton  beforeLogout={toggle}/>
            <div className="LogoutButton" onClick={toggle}>
              Close
            </div>
            </div>
          </Fragment>
        </DialogContent>
      </Dialog>
    </Fragment>
  );
}
