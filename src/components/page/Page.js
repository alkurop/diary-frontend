//libs
import React from "react";
import { withUserAgent } from "react-useragent";
import { connect } from "react-redux";

//comps
import StoryComponent from "../story/StoryComponent";
import SideMenuWrapper from "../side_menu/SideMenuWrapper";
import Contacts from "../contacts/Contacts";

import "./Page.scss";
import "../side_menu/SideMenu.scss";

function Page({ isLoaded, ua, page }) {
  const isMobile = ua.mobile;
  let filterId;
  if (page.owner) {
    filterId = page.owner.id;
  }
  return (
    <div className="Page">
      <SideMenuWrapper />
      <StoryComponent isLoaded={isLoaded} />
      <div>{isMobile ? null : <Contacts filterId={filterId} />}</div>
    </div>
  );
}

export default withUserAgent(connect(state => ({ page: state.page }))(Page));
