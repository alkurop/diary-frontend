import React from "react";

function AudioSelector({ onAudioSelected }) {
  const [isTooBig, setTooBig] = React.useState(false);
  function onChange(e) {
    const file = e.target.files[0];
    if (file.size > MAX_IMAGE_SIZE_BYTES) {
      setTooBig(true);
    } else {
      setTooBig(false);
      onAudioSelected(file);
    }
  }
  return (
    <div className="ImageSelector">
      <input type="file" accept="audio/*" onChange={e => onChange(e)} />

      {isTooBig ? (
        <div className="ImageSelectorError">Audio is bigger then 10mb</div>
      ) : null}
    </div>
  );
}
export default AudioSelector;
const MAX_IMAGE_SIZE_BYTES = 10000000; //10mb
