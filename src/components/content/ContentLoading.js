import React from "react";
import "./Content.scss"

export default function ContentLoading() {
  return (
    <div className="ContentLoading">
      Loading ...
    </div>
  );
}
